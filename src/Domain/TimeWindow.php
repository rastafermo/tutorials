<?php
declare(strict_types=1);

namespace Tutorials\Domain;

final class TimeWindow
{
    private const THRESHOLD = 100;
    public const OPEN = 'open';
    public const CLOSED = 'closed';

    private $usedCapacity;

    public function __construct(int $usedCapacity)
    {
        $this->usedCapacity = $usedCapacity;
    }

    public function getState(): string
    {
        if ($this->usedCapacity < self::THRESHOLD) {
            return self::OPEN;
        }

        return self::CLOSED;
    }
}
