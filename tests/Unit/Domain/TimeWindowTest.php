<?php
declare(strict_types=1);

namespace Tests\Unit\Domain;

use Tests\TestCase;
use Tutorials\Domain\TimeWindow;

class TimeWindowTest extends TestCase
{
    public function testShouldOpenTimeWindow(): void
    {
        $timeWindow = new TimeWindow(50);
        self::assertEquals(TimeWindow::OPEN, $timeWindow->getState());
    }

    public function testShouldCloseTimeWindow(): void
    {
        $timeWindow = new TimeWindow(150);
        self::assertEquals(TimeWindow::CLOSED, $timeWindow->getState());
    }
}
